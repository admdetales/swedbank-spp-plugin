<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\SwedbankSpp\Payum\Action\CreditCard\Api;

use Omni\Sylius\SwedbankSpp\Communication\AbstractCommunication;
use Payum\Core\Exception\UnsupportedApiException;

trait ApiAwareTrait
{

    /**
     * @var AbstractCommunication
     */
    protected $communication;

    /**
     * @param $communication
     */
    public function setApi($communication): void
    {
        if (false === $communication instanceof AbstractCommunication) {
            throw new UnsupportedApiException('Not supported.Expected an instance of ' . AbstractCommunication::class);
        }

        $this->communication = $communication;
    }
}
