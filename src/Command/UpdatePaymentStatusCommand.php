<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\SwedbankSpp\Command;

use Omni\Sylius\SwedbankSpp\Doctrine\ORM\PaymentRepository;
use Omni\Sylius\SwedbankSpp\Resolver\PaymentStateResolver;
use Sylius\Component\Core\Model\PaymentInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdatePaymentStatusCommand extends Command
{
    /**
     * @var PaymentRepository
     */
    private $paymentRepository;

    /**
     * @var PaymentStateResolver
     */
    private $paymentStateResolver;

    /**
     * @param PaymentRepository $paymentRepository
     * @param PaymentStateResolver $paymentStateResolver
     */
    public function __construct(PaymentRepository $paymentRepository, PaymentStateResolver $paymentStateResolver)
    {
        parent::__construct();

        $this->paymentRepository = $paymentRepository;
        $this->paymentStateResolver = $paymentStateResolver;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setName('omni:swedbank-spp:update-payment-state')
            ->setDescription('Updates the payments state.')
            ->setHelp('This command allows you to update the payments state for Swedbank SPP gateway.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $payments = $this->paymentRepository->findAllByGatewayFactoryNameAndState(
            'swedbank_spp',
            [PaymentInterface::STATE_AUTHORIZED]
        );

        foreach ($payments as $payment) {
            $this->paymentStateResolver->resolve($payment);
        }
    }
}
